# Dungeon Defense

## Initial Planning
- The project is going to be open sourced.
- Project is going to be made with OpenGL and C++
- The project will be about defending a dungeon
- It's going to be a kind of tower defense game
    - Player could be some kind of creature
- Basic options are going to be such as:
    - You can build building that defends your dungeon
    - You can build building that spawns little creatures to fight(suicide bomb) for you.
    - You can build building that speeds up progress of the buildings
    - You can build building that spawns various creatures
- Since because it's a turn based tower defense kind of game, it's going to include turns and waves that varies each time
- The game should contain grid kind of terrain to locate buildings and maybe  spawns
- The game is going to have MOBA style camera view
- Enemies should came from the right upper side of the screen
- The services that are going to used during development are : 
    - Gitlab
    - Toggle
