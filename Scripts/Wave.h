#ifndef WAVE_H
#define WAVE_H

#include <global.h>
#include <Minion.h>
#include <vector>

class Wave{
public:
    float distance;         // Distance between two minions
    vector<Minion*> minions;

    Wave();
    void Update();

    void RemoveMinion(Minion *current);
    void RemoveMinionFromObject(DynamicObject *current);
private:
    int counter = 0;
    bool full = false;
    int debug = 1;
};

Wave::Wave(){
    distance = 5.0f;
    minions.push_back(new Minion);  // first minion
}
void Wave::Update(){
    if(!full){
        if(minions.back()->minionObject->Position.z > START_POSITION.z + distance){
            minions.push_back(new Minion);
            counter ++;
        }
        if(counter >= QUANTITY){
            full = true;
        }
    }

    for(int i = 0; i < minions.size(); i++){
        minions[i]->Update();
    }
    currentAliveMinion = (int)minions.size();
    waveStatusPercent = currentAliveMinion/QUANTITY;

    if(full && minions.size() == 0){
        waveStarted = false;
    }
}

void Wave::RemoveMinion(Minion *current){
    // If someone already removed the minion
    for(unsigned int i = 0; i < minions.size(); i++){
        if(minions[i] == current){
            vector<Minion*>::iterator deleted = remove(minions.begin(), minions.end(), current);
            minions.erase(deleted);
            delete current;
            killedMinionCounter++;
            break;
        }
    }
}
void Wave::RemoveMinionFromObject(DynamicObject *current){
    Minion *removedMinion;
    for(unsigned int i = 0; i < minions.size(); i++) {
        if(minions[i]->GetMinionObject() == current){
            removedMinion = minions[i];
            break;
        }
    }
    if(removedMinion)
        RemoveMinion(removedMinion);
}

#endif //PROJECT_WAVE_H
