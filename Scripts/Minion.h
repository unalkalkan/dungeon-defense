#ifndef MINION_H
#define MINION_H

#include <DynamicObject.h>

class Minion{
public:
    float Health;
    glm::vec3 Target;
    DynamicObject *minionObject;

    Minion();
    ~Minion();
    void Update();

    bool GetHit(float damage);
    void SetTarget(glm::vec3 target);
    glm::vec3 GetPosition();
    float GetDistance(glm::vec3 posFrom);
    DynamicObject* GetMinionObject();
private:
    glm::vec3 currentTarget;
};

Minion::Minion(){
    minionObject = new DynamicObject((char*)"Models/MonsterB/MonsterB.obj", btVector3(START_POSITION.x, START_POSITION.y, START_POSITION.z), btVector3(1.0f, 1.0f, 1.0f));
    minionObject->Rotate(glm::quat(glm::vec3(0.0f, 210.0f, 0.0f)));
    minionObject->ScaleModel(glm::vec3(0.7f, 0.7f, 0.7f));
    minionObject->SetType((char*)"MinionObject");
    SetTarget(TARGET_POSITION);
    Health = MINION_HEALTH;
}
Minion::~Minion(){
    physicsEngine.dynamicsWorld->removeRigidBody(minionObject->rigidBody);
    delete minionObject;
}
void Minion::Update(){
    // FIXME: Remove this if line
    if(minionObject){
        minionObject->Speed = MINION_SPEED;
        minionObject->Update();

        if(Target != currentTarget){
            minionObject->SmoothMove(btVector3(Target.x, Target.y, Target.z));
            currentTarget = Target;
        }
    }
}
void Minion::SetTarget(glm::vec3 target) {
    Target = target;
}

bool Minion::GetHit(float damage){
    Health = Health - damage;
    if(Health <= 0){
        // Destroy minion
        cout << "Minion killed." << endl;
        return true;
    }
    return false;
}
glm::vec3 Minion::GetPosition(){
    if(minionObject->rigidBody){
        return minionObject->Position;
    }
    return glm::vec3();
}
float Minion::GetDistance(glm::vec3 posFrom) {
    if(minionObject->rigidBody){
        return glm::distance2(posFrom, minionObject->Position);
    }else{
        return -1;
    }
}
DynamicObject* Minion::GetMinionObject(){
    if(minionObject){
        return minionObject;
    }
    return nullptr;
}
#endif //MINION_H
