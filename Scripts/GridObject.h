#ifndef GRIDOBJECT_H
#define GRIDOBJECT_H

#include <GameObject.h>
#include <DynamicObject.h>
#include <GhostObject.h>
#include <global.h>

#include <Minion.h>
#include <Wave.h>

class GridObject: public GameObject{
public:
    glm::vec3 BARREL;
    bool empty = true;

    DynamicObject *tower;
    GhostObject *placeholder;
    Minion* targetMinion = NULL;
    Wave* currentWave;

    void InitWave(Wave* _currentWave);
    void AddTower();
    void RemoveTower();
    void Attack();
    void RenderUpdate();

    GridObject(btVector3 position);
    void LogicUpdate();
    char* GetType();

private:
    float towerDistanceLength;
    void HandleTarget();
    bool ControlTargetValidity();
    void InstantiateBullet();
    vector<DynamicObject*> bullets;

    // FindNearestMinion() Properties
    void FindNearestMinion();
    glm::vec3 minionPos;
    float minionDistance;
    int shortestIndex = -1;
    float shortestDistance = -1;
    // ----------------
    // Attack() Properties
    float cooldown = COOLDOWN;
};
GridObject::GridObject(btVector3 position) {
    // Initialization of the script
    Position = glm::vec3(position.getX(), position.getY(), position.getZ());
    towerDistanceLength = glm::distance2(Position, Position + TOWER_RADIUS);
    BARREL = glm::vec3(Position.x, 8, Position.z);

    // Setting placeholder.
    placeholder = new GhostObject((char*)"Models/EmptySlot/placeholder.obj", position, btVector3(2.0f, 0.33f, 2.0f));
    placeholder->SetType((char*)"GridObject");
    placeholder->parent = this;
}
char* GridObject::GetType() {
    return (char *)"GridObject";
}

void GridObject::InitWave(Wave *_currentWave) {
    currentWave = _currentWave;
}
void GridObject::AddTower() {
    if(empty && BoughtTurret > 0){
        empty = false;
        tower = new DynamicObject((char*)"Models/Turret/Turret.fbx", btVector3(Position.x, Position.y, Position.z), btVector3(1.0f, 1.0f, 1.0f));
        tower->ScaleModel(glm::vec3(0.5f, 0.5f, 0.5f));
        glm::quat rotate;
        if(Position.x < 0){
            rotate = glm::quat(glm::vec3(0.0f, 90.0f, 0.0f));
        }else{
            rotate = glm::quat(glm::vec3(0.0f, -90.0f, 0.0f));
        }
        tower->Rotate(rotate);
        BoughtTurret--;
    }
}
void GridObject::RemoveTower() {
    if(!empty){
        // removing
        empty = true;
        cout << "Tower deleted" << endl;
        physicsEngine.dynamicsWorld->removeRigidBody(tower->rigidBody);
        delete tower;
    }
}
void GridObject::RenderUpdate(){
    if(!empty) {
        tower->Update();
    }
    if(empty && BoughtTurret > 0){
        placeholder->Update();
    }
}

void GridObject::LogicUpdate(){
    if(!empty){
        HandleTarget();
    }
}

bool GridObject::ControlTargetValidity(){
    for(int i = 0; i < currentWave->minions.size(); i++){
        if(targetMinion == currentWave->minions[i]){
            return true;
        }
    }
    return false;
}
void GridObject::HandleTarget(){
    if(targetMinion == nullptr){
        // Search for a new target
        FindNearestMinion();
    }else{
        if(!ControlTargetValidity()){
            targetMinion = nullptr;
            return;
        }
        DebugLine(BARREL, targetMinion->GetPosition(), glm::vec3(0.0f, 0.0f, 0.0f));
        // Control target minion's distance

        float minionDistance = targetMinion->GetDistance(tower->Position);
        if(minionDistance > towerDistanceLength){
            targetMinion = nullptr;
        }
        Attack();
    }
}
void GridObject::FindNearestMinion() {
    shortestIndex = -1;
    shortestDistance = -1;
    for(unsigned int i = 0; i < currentWave->minions.size(); i++){
        minionPos = currentWave->minions[i]->minionObject->Position;
        minionDistance = glm::distance2(tower->Position, minionPos);

        if(minionDistance < towerDistanceLength){
            if(minionDistance > shortestDistance){
                shortestDistance = minionDistance;
                shortestIndex = i;
            }
        }
    }

    if(shortestIndex != -1){
        targetMinion = currentWave->minions[shortestIndex];
    }
}

void GridObject::Attack(){
    if(targetMinion != nullptr){
        if(cooldown <= 0){
            cooldown = COOLDOWN;
            // Bullet throwing is kind of an option right now.
            // InstantiateBullet();
            bool killed = targetMinion->GetHit(DAMAGE);
            if(killed){
                currentWave->RemoveMinion(targetMinion);
                targetMinion = nullptr;
                Money += GOLD_PER_MINION;
            }
        }else{
            cooldown -= 0.1f;
        }
    }else{
        cooldown = COOLDOWN;
    }
}

void GridObject::InstantiateBullet(){
    DynamicObject *bullet = new DynamicObject((char*)"Models/Sphere/Sphere.obj",
                                              btVector3(BARREL.x, BARREL.y, BARREL.z), btVector3(0.2f, 0.2f, 0.2f));
    bullet->mass = 0.1f;
    bullet->Speed = 150.0f;
    bullet->ScaleModel(glm::vec3(0.2f, 0.2f, 0.2f));
    bullet->SmoothMove(btVector3(targetMinion->minionObject->Position.x, targetMinion->minionObject->Position.y, targetMinion->minionObject->Position.z));
    bullets.push_back(bullet);
}
#endif //GRIDOBJECT_H
