cmake_minimum_required(VERSION 3.1.3)

set(GLFW_SOURCE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/external/glfw")
set(GLM_SOURCE_DIR "external/glm")
set(GLAD_SOURCE_DIR "external/glad")
set(ASSIMP_SOURCE_DIR "external/assimp")
set(BULLET_SOURCE_DIR "external/bullet")
set(NANOGUI_SOURCE_DIR "external/nanogui")

# Building only the GLFW lib
set(BUILD_SHARED_LIBS OFF CACHE BOOL "")
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "")
set(GLFW_BUILD_TESTS OFF CACHE BOOL "")
set(GLFW_BUILD_DOCS OFF CACHE BOOL "")
set(GLFW_INSTALL OFF CACHE BOOL "")

add_subdirectory(${GLFW_SOURCE_DIR} ${GLAD_SOURCE_DIR})

include_directories(
        ${GLAD_SOURCE_DIR}/include
        ${GLFW_SOURCE_DIR}/include
        ${GLFW_SOURCE_DIR}/deps
        ${GLM_SOURCE_DIR}
        ${ASSIMP_SOURCE_DIR}/include
        ${BULLET_SOURCE_DIR}/src
        )

set(ASSIMP_BUILD_STATIC_LIB ON)
add_subdirectory(${ASSIMP_SOURCE_DIR})
add_subdirectory(${BULLET_SOURCE_DIR})

add_library(glad STATIC
        ${GLAD_SOURCE_DIR}/src/glad.c
        ${GLAD_SOURCE_DIR}/include
        )

target_link_libraries(glad ${GLFW_LIBRARIES})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")

if (APPLE)
    set(APP_NAME "dungeon-defense")

    set(CMAKE_CXX_STANDARD 11)
    set(CMAKE_CXX_STANDARD_REQUIRED ON)  # not necessary, but encouraged
    if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/icon.icns)
        add_executable(${APP_NAME} MACOSX_BUNDLE main.cpp icon.icns)
        set_source_files_properties("icon.icns" PROPERTIES MACOSX_PACKAGE_LOCATION "Resources")
        set_target_properties(${APP_NAME} PROPERTIES MACOSX_BUNDLE_ICON_FILE ${CMAKE_CURRENT_SOURCE_DIR}/icon.icns)
    else ()
        add_executable(${APP_NAME} MACOSX_BUNDLE main.cpp)
    endif ()

    set_target_properties(${APP_NAME} PROPERTIES MACOSX_BUNDLE_INFO_PLIST ${CMAKE_CURRENT_SOURCE_DIR}/CMakeModules/app.plist.in)

    set_target_properties(${APP_NAME} PROPERTIES MACOSX_BUNDLE_BUNDLE_NAME ${APP_NAME})
    set_target_properties(${APP_NAME} PROPERTIES MACOSX_BUNDLE_SHORT_VERSION_STRING "1.0")
    set_target_properties(${APP_NAME} PROPERTIES MACOSX_BUNDLE_LONG_VERSION_STRING "1.0")
    set_target_properties(${APP_NAME} PROPERTIES MACOSX_BUNDLE_INFO_STRING "OpenGL boilerplate example app")
else()
    set(CMAKE_CXX_STANDARD 11)
    set(CMAKE_CXX_STANDARD_REQUIRED ON)  # not necessary, but encouraged
    set(APP_NAME "dungeon-defense")
    add_executable(${APP_NAME} main.cpp)
endif()

set(HEADERS "include/")
set(SCRIPTS "Scripts/")
include_directories(${HEADERS})
include_directories(${SCRIPTS})

set(NANOGUI_BUILD_EXAMPLE OFF CACHE BOOL " " FORCE)
set(NANOGUI_BUILD_PYTHON  OFF CACHE BOOL " " FORCE)
set(NANOGUI_BUILD_SHARED  OFF CACHE BOOL " " FORCE)
set(NANOGUI_INSTALL       ON CACHE BOOL " " FORCE)

add_subdirectory(${NANOGUI_SOURCE_DIR})
include_directories(${NANOGUI_SOURCE_DIR}/ext/eigen
                    ${NANOGUI_SOURCE_DIR}/ext/nanovg/src
                    ${NANOGUI_SOURCE_DIR}/include
                    )

file(COPY Models DESTINATION ${CMAKE_BINARY_DIR})
file(COPY Scene DESTINATION ${CMAKE_BINARY_DIR})
file(COPY Shaders DESTINATION ${CMAKE_BINARY_DIR})
file(COPY Textures DESTINATION ${CMAKE_BINARY_DIR})

target_link_libraries(${APP_NAME} glad glfw ${GLFW_LIBRARIES} assimp pybullet nanogui)
