#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <btBulletDynamicsCommon.h>
#include <GLDebugDrawer.h>

#include <GUI.h>
#include <StaticObject.h>

//#include <GameObject.h>
//#include <DynamicObject.h>
//#include <Minion.h>
//#include <Wave.h>
//#include <TextureLoad.h>
//#include <Model.h>
//#include <Shader.h>
//#include <Camera.h>
//#include <Physics.h>
//#include <global.h>

using namespace std;

// Spesific Definitions
bool firstMouse = true;
float lastX = screenWidth/2;
float lastY = screenHeight/2;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void ProcessInput(GLFWwindow *window);
void mouse_callback(GLFWwindow *window, double xpos, double ypos);
void keyboard_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods);
void UpdateScene();
void UpdateLightning(Shader lightningShader);
void ProcessCameraMovement(double xpos, double ypos);
void ScreenPosToWorldRay(glm::vec3 &out_origin, glm::vec3 &out_direction);
void TestRaycastHit(GLFWwindow *window);
void IdentifyObject(DynamicObject *current, const glm::vec3 &color);
void IdentifySelected();
void PerformCollisionDetection();

//Screen
nanogui::Screen *screen = nullptr;

//Experimental
StaticObject *staticScene;
Shader *outlineShader;
btCollisionWorld::ClosestRayResultCallback *RayCallback;

DynamicObject *baseObject;

int main( int argc, char** argv){
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4.5);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(screenWidth, screenHeight, "Dungeon Defense - Main Window", NULL, NULL);
    if(window == NULL){
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)){
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    screen = new nanogui::Screen();
    screen->initialize(window, true);
    InitGUI(screen);

    // GLFW Callbacks
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetKeyCallback(window, keyboard_callback); // The keyboard callback function is for single pressed keys.
    glfwSetMouseButtonCallback(window, mouse_button_callback); // Mouse button callback

    glfwSetCharCallback(window, [](GLFWwindow *, unsigned int codepoint) {
                            screen->charCallbackEvent(codepoint);
                        }
    );

    glfwSetDropCallback(window, [](GLFWwindow *, int count, const char **filenames) {
                            screen->dropCallbackEvent(count, filenames);
                        }
    );

    glfwSetScrollCallback(window, [](GLFWwindow *, double x, double y) {
                              screen->scrollCallbackEvent(x, y);
                          }
    );

    // Enable Depth Test and Stencil Test
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_STENCIL_TEST);
    glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

    // Physics
    physicsEngine.InitPhysics();
    physicsEngine.SetGravity();
    GLDebugDrawer *debugDraw;
    debugDraw = new GLDebugDrawer((char*)"Shaders/LineVertexShader.glsl", (char*)"Shaders/LineFragmentShader.glsl");
    physicsEngine.dynamicsWorld->setDebugDrawer(debugDraw);

    unsigned int VBO;
    glGenBuffers(1, &VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);

    //--CubeVAO
    glGenVertexArrays(1, &cubeVAO);
    glBindVertexArray(cubeVAO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), cubeVertices, GL_STATIC_DRAW);

    // Position Attributes
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    // TextureLoad Coord Attributes
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3* sizeof(float)));
    glEnableVertexAttribArray(1);
    // Normal Vectors Attributes
    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(5* sizeof(float)));
    glEnableVertexAttribArray(2);
    //--CubeVAO

    //--Light VAO
    glGenVertexArrays(1, &lightVAO);
    glBindVertexArray(lightVAO);

    // Position Attributes
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    //--Light VAO

    // Wireframe Mode
    // glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

    // Shaders
    Shader lightningShader((char*)"Shaders/LightSourceVertexShader.glsl", (char*)"Shaders/LightSourceFragmentShader.glsl");
    lineShader = new Shader((char*)"Shaders/LineVertexShader.glsl", (char*) "Shaders/LineFragmentShader.glsl");
    outlineShader = new Shader((char*)"Shaders/ModelVertexShader.glsl", (char*)"Shaders/OutlineFragmentShader.glsl");

    // Initialization of the ground and static scene.
    staticScene = new StaticObject((char*)"Scene/Scene.obj");

    baseObject = new DynamicObject("Models/Cube/cube.obj", btVector3(-0.1f, 1.0f, 25.0118f), btVector3(8.0f, 1.0f, 8.0f), 0);
    baseObject->SetType((char*)"BaseObject");

    // Initialization the grid slots.
    float z_spacing = 4.0f;
    float x_spacing = 5.0f;
    for(int i = 0; i < 11; i++){
        for(int j = 0; j < 2; j++){
            gridObjects.push_back(new GridObject(btVector3(-5.0f + (j * x_spacing * 2), 0.0f, -40.0f + (i * z_spacing))));
        }
        z_spacing = 5.0f;
    }


    // Render loop
    while(!glfwWindowShouldClose(window)){
        // Input handling
        ProcessInput(window);

        // FrameRate and DeltaTime calculations
        float currentFrame = (float)glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        physicsEngine.dynamicsWorld->stepSimulation(deltaTime);

        // Rendering
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_STENCIL_TEST);

        // Projection and View settings
        view = camera.GetViewMatrix();  // Get the information from camera itself
        projection = glm::perspective(glm::radians(45.0f), (float)screenWidth/(float)screenHeight, 0.1f, 100.0f);

        // Physics Debug Draw
        debugDraw->InitShader(view, projection);
//        physicsEngine.dynamicsWorld->debugDrawWorld();

        // Update Plane
        glStencilMask(0x00);
        UpdateScene();

        // Update Objects
        glStencilFunc(GL_ALWAYS, 1, 0xFF); // all fragments should update the stencil buffer
        glStencilMask(0xFF); // enable writing to the stencil buffer

        // Control ray hits and identify selected.
        TestRaycastHit(window);
        IdentifySelected();

        for(int i = 0; i < gridObjects.size(); i++){
            if(waveStarted){
                gridObjects[i]->LogicUpdate();
            }
            gridObjects[i]->RenderUpdate();
        }
        if(waveStarted){
            currentWave->Update();
        }

        PerformCollisionDetection();
        // Update Scene Lightnings
        //UpdateLightning(lightningShader);

        // Render GUI
        screen->drawContents();
        screen->drawWidgets();
        UpdateGUI();

        // Checks if any events are triggered and swap the buffers
        glfwSwapBuffers(window);
        glfwPollEvents();   
    }
    glfwTerminate();

    physicsEngine.DeleteResources();
    return 0;
}
void UpdateScene(){
    staticScene->Update();
}
void UpdateLightning(Shader lightningShader){
    lightningShader.use();
    lightningShader.setMat4("view", view);
    lightningShader.setMat4("projection", projection);

    glBindVertexArray(lightVAO);
    for(unsigned int i = 0; i < 4; i++){
        // Point Light Container and Settings
        model = glm::mat4();
        model = glm::translate(model, pointLightPositions[i]);
        model = glm::scale(model, glm::vec3(0.2f));
        lightningShader.setMat4("model", model);

        glDrawArrays(GL_TRIANGLES, 0, 36);
    }
//    glBindVertexArray(lightVAO);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height){
    screen->resizeCallbackEvent(width, height);
    glViewport(0, 0, width, height);
}
void ProcessInput(GLFWwindow *window){
    // Window closing
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS){
        glfwSetWindowShouldClose(window, true);
    }

    // Camera movement
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.ProcessKeyboard(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.ProcessKeyboard(RIGHT, deltaTime);

    // Camera Speed
    if(glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS){
        camera.MovementSpeed = SPEED * 2;
    }else{
        camera.MovementSpeed = SPEED;
    }
}
void mouse_callback(GLFWwindow *window, double xpos, double ypos){
    screen->cursorPosCallbackEvent(xpos, ypos);
    mouse_x = (float)xpos;
    mouse_y = (float)ypos;
    ProcessCameraMovement(xpos, ypos);
}
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods){
    screen->mouseButtonCallbackEvent(button, action, mods);
    if(button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS){
        if(hovered != nullptr){
            selected = hovered;
        }
        if(hoveredGrid != nullptr){
            if(hoveredGrid->empty)
                hoveredGrid->AddTower();
            else{
                if(selected == hoveredGrid->tower){
                    selected = nullptr;
                }
                hovered = nullptr;
//                hoveredGrid->RemoveTower();
                hoveredGrid = nullptr;
            }
        }
    }

//    if(button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS){
//        if(selected != nullptr){
//            btVector3 selectedPosition(RayCallback->m_hitPointWorld.getX(), selected->Position.y, RayCallback->m_hitPointWorld.getZ());
//            selected->SmoothMove(selectedPosition);
//            cout << selected->Position.x << endl;
//        }
//    }
}
void keyboard_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
    screen->keyCallbackEvent(key, scancode, action, mods);
    // Change camera type
    if(key == GLFW_KEY_T && action == GLFW_PRESS){
        camera.ChangeCameraType();
        if(camera.CameraType == FREE)
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        else
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    }
}
void ProcessCameraMovement(double xpos, double ypos){
    if(firstMouse){
        lastX = (float)xpos;
        lastY = (float)ypos;
        firstMouse = false;
    }
    float xoffset = (float)xpos - lastX;
    float yoffset = lastY - (float)ypos;
    lastX = (float)xpos;
    lastY = (float)ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}
void ScreenPosToWorldRay(glm::vec3 &out_origin, glm::vec3 &out_direction){
    glm::vec4 lRayStart_NDC(
            (mouse_x/(float)screenWidth  - 0.5f) * 2.0f, // [0,1024] -> [-1,1]
            1.0f - (2.0f * mouse_y) / screenHeight, // [0, 768] -> [-1,1]
            -1.0, // The near plane maps to Z=-1 in Normalized Device Coordinates
            1.0f
    );
    glm::vec4 lRayEnd_NDC(
            (mouse_x/(float)screenWidth  - 0.5f) * 2.0f,
            1.0f - (2.0f * mouse_y) / screenHeight,
            0.0,
            1.0f
    );
    glm::mat4 InverseProjectionMatrix = glm::detail::compute_inverse(projection);
    glm::mat4 InverseViewMatrix = glm::detail::compute_inverse(view);

    glm::vec4 lRayStart_camera = InverseProjectionMatrix * lRayStart_NDC;
    lRayStart_camera /= lRayStart_camera.w;
    glm::vec4 lRayStart_world  = InverseViewMatrix * lRayStart_camera;

    lRayStart_world /= lRayStart_world .w;
    glm::vec4 lRayEnd_camera   = InverseProjectionMatrix * lRayEnd_NDC;
    lRayEnd_camera /= lRayEnd_camera  .w;
    glm::vec4 lRayEnd_world    = InverseViewMatrix * lRayEnd_camera;
    lRayEnd_world /= lRayEnd_world   .w;

    glm::vec3 lRayDir_world(lRayEnd_world - lRayStart_world);
    lRayDir_world = glm::normalize(lRayDir_world);

    out_origin = glm::vec3(lRayStart_world);
    out_direction = glm::normalize(lRayDir_world);
}

void TestRaycastHit(GLFWwindow *window){
    glm::vec3 out_origin;
    glm::vec3 out_direction;
    ScreenPosToWorldRay(out_origin, out_direction);

    glm::vec3 out_end = out_origin + out_direction * 1000.0f;
    btVector3 start = btVector3(out_origin.x, out_origin.y, out_origin.z);
    btVector3 end = btVector3(out_end.x, out_end.y, out_end.z);

    RayCallback = new btCollisionWorld::ClosestRayResultCallback(start, end);
    physicsEngine.dynamicsWorld->rayTest(start, end, *RayCallback);

    if(RayCallback->hasHit()) {
        GameObject* p = (GameObject *)RayCallback->m_collisionObject->getUserPointer();
        if(p){
//            cout << p->GetType() << endl;
            if(p->GetType() == "DynamicObject"){
                hovered = (DynamicObject*)p;
                IdentifyObject(hovered, glm::vec3(0.70f, 0.70f, 0.70f));
            }
            if(p->GetType() == "GridObject"){
                hoveredGrid = (GridObject*)p->parent;
            }else{
                hoveredGrid = nullptr;
            }
        }
        // Debug Ray Hit
//        btVector3 debugPos = RayCallback->m_hitPointWorld;
//        DebugLine(glm::vec3(debugPos.getX(), 0, debugPos.getZ()), glm::vec3(debugPos.getX(), 50, debugPos.getZ()), glm::vec3(1.0f, 1.0f, 1.0f));
    }else{
//        cout << "Nothing is picked " << endl;
    }
}
void IdentifyObject(DynamicObject *current, const glm::vec3 &color) {
    glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
    glStencilMask(0x00);
    glDisable(GL_DEPTH_TEST);

    outlineShader->use();
    outlineShader->setVec3("outlineColor", color);
    current->Update(*outlineShader);

    glStencilMask(0xFF);
    glEnable(GL_DEPTH_TEST);
}
void IdentifySelected(){
    if(selected != nullptr){
        IdentifyObject(selected, glm::vec3(0.95f, 0.67f, 0.25f));
    }
}

void PerformCollisionDetection(){
    //Perform collision detection
    physicsEngine.dynamicsWorld->performDiscreteCollisionDetection();

    int numManifolds = physicsEngine.dynamicsWorld->getDispatcher()->getNumManifolds();
    //For each contact manifold
    for (int i = 0; i < numManifolds; i++) {
        btPersistentManifold* contactManifold = physicsEngine.dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(i);
        const btCollisionObject* obA = contactManifold->getBody0();
        const btCollisionObject* obB = contactManifold->getBody1();

        GameObject *obj1 = (GameObject*)obA->getUserPointer();
        GameObject *obj2 = (GameObject*)obB->getUserPointer();

        if((obj1->GetType() == (char*)"BaseObject" && obj2->GetType() == (char*)"MinionObject") ||
           (obj2->GetType() == (char*)"BaseObject" && obj1->GetType() == (char*)"MinionObject")){
            // Base is under attack.
            DynamicObject *deleted;
            BASE_HEALTH = BASE_HEALTH - MINION_DAMAGE;
            if(obj2->GetType() == (char*)"MinionObject"){
                deleted = (DynamicObject*)obj2;
                obj2 = nullptr;
            }else{
                deleted = (DynamicObject*)obj1;
                obj1 = nullptr;
            }
            if(deleted){
                cout << "Minion deleted." << endl;
                currentWave->RemoveMinionFromObject(deleted);
            }
            continue;
        }
    }
}