# CMake generated Testfile for 
# Source directory: /home/rothnald/dungeon-defense/external/assimp/test
# Build directory: /home/rothnald/dungeon-defense/external/assimp/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(unittests "unit")
subdirs("headercheck")
