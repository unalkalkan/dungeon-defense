# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/rothnald/dungeon-defense/external/bullet/examples/SimpleOpenGL3/main.cpp" "/home/rothnald/dungeon-defense/external/bullet/examples/SimpleOpenGL3/CMakeFiles/AppSimpleOpenGL3.dir/main.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GLEW_DYNAMIC_LOAD_ALL_GLX_FUNCTIONS=1"
  "GLEW_INIT_OPENGL11_FUNCTIONS=1"
  "GLEW_STATIC"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/home/rothnald/dungeon-defense/external/bullet/src"
  "/home/rothnald/dungeon-defense/external/bullet/btgui"
  "/home/rothnald/dungeon-defense/external/bullet/examples"
  "/home/rothnald/dungeon-defense/external/bullet/examples/ThirdPartyLibs/Glew"
  "/home/rothnald/dungeon-defense/external/bullet/btgui/OpenGLWindow/GlewWindows"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
