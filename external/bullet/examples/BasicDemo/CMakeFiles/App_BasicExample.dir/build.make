# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.9

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/local/bin/cmake

# The command to remove a file.
RM = /usr/local/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo

# Include any dependencies generated for this target.
include CMakeFiles/App_BasicExample.dir/depend.make

# Include the progress variables for this target.
include CMakeFiles/App_BasicExample.dir/progress.make

# Include the compile flags for this target's objects.
include CMakeFiles/App_BasicExample.dir/flags.make

CMakeFiles/App_BasicExample.dir/BasicExample.o: CMakeFiles/App_BasicExample.dir/flags.make
CMakeFiles/App_BasicExample.dir/BasicExample.o: BasicExample.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object CMakeFiles/App_BasicExample.dir/BasicExample.o"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/App_BasicExample.dir/BasicExample.o -c /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo/BasicExample.cpp

CMakeFiles/App_BasicExample.dir/BasicExample.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/App_BasicExample.dir/BasicExample.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo/BasicExample.cpp > CMakeFiles/App_BasicExample.dir/BasicExample.i

CMakeFiles/App_BasicExample.dir/BasicExample.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/App_BasicExample.dir/BasicExample.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo/BasicExample.cpp -o CMakeFiles/App_BasicExample.dir/BasicExample.s

CMakeFiles/App_BasicExample.dir/BasicExample.o.requires:

.PHONY : CMakeFiles/App_BasicExample.dir/BasicExample.o.requires

CMakeFiles/App_BasicExample.dir/BasicExample.o.provides: CMakeFiles/App_BasicExample.dir/BasicExample.o.requires
	$(MAKE) -f CMakeFiles/App_BasicExample.dir/build.make CMakeFiles/App_BasicExample.dir/BasicExample.o.provides.build
.PHONY : CMakeFiles/App_BasicExample.dir/BasicExample.o.provides

CMakeFiles/App_BasicExample.dir/BasicExample.o.provides.build: CMakeFiles/App_BasicExample.dir/BasicExample.o


CMakeFiles/App_BasicExample.dir/main.o: CMakeFiles/App_BasicExample.dir/flags.make
CMakeFiles/App_BasicExample.dir/main.o: main.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Building CXX object CMakeFiles/App_BasicExample.dir/main.o"
	/usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/App_BasicExample.dir/main.o -c /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo/main.cpp

CMakeFiles/App_BasicExample.dir/main.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/App_BasicExample.dir/main.i"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo/main.cpp > CMakeFiles/App_BasicExample.dir/main.i

CMakeFiles/App_BasicExample.dir/main.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/App_BasicExample.dir/main.s"
	/usr/bin/c++ $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo/main.cpp -o CMakeFiles/App_BasicExample.dir/main.s

CMakeFiles/App_BasicExample.dir/main.o.requires:

.PHONY : CMakeFiles/App_BasicExample.dir/main.o.requires

CMakeFiles/App_BasicExample.dir/main.o.provides: CMakeFiles/App_BasicExample.dir/main.o.requires
	$(MAKE) -f CMakeFiles/App_BasicExample.dir/build.make CMakeFiles/App_BasicExample.dir/main.o.provides.build
.PHONY : CMakeFiles/App_BasicExample.dir/main.o.provides

CMakeFiles/App_BasicExample.dir/main.o.provides.build: CMakeFiles/App_BasicExample.dir/main.o


# Object files for target App_BasicExample
App_BasicExample_OBJECTS = \
"CMakeFiles/App_BasicExample.dir/BasicExample.o" \
"CMakeFiles/App_BasicExample.dir/main.o"

# External object files for target App_BasicExample
App_BasicExample_EXTERNAL_OBJECTS =

App_BasicExample: CMakeFiles/App_BasicExample.dir/BasicExample.o
App_BasicExample: CMakeFiles/App_BasicExample.dir/main.o
App_BasicExample: CMakeFiles/App_BasicExample.dir/build.make
App_BasicExample: CMakeFiles/App_BasicExample.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo/CMakeFiles --progress-num=$(CMAKE_PROGRESS_3) "Linking CXX executable App_BasicExample"
	$(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/App_BasicExample.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
CMakeFiles/App_BasicExample.dir/build: App_BasicExample

.PHONY : CMakeFiles/App_BasicExample.dir/build

CMakeFiles/App_BasicExample.dir/requires: CMakeFiles/App_BasicExample.dir/BasicExample.o.requires
CMakeFiles/App_BasicExample.dir/requires: CMakeFiles/App_BasicExample.dir/main.o.requires

.PHONY : CMakeFiles/App_BasicExample.dir/requires

CMakeFiles/App_BasicExample.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/App_BasicExample.dir/cmake_clean.cmake
.PHONY : CMakeFiles/App_BasicExample.dir/clean

CMakeFiles/App_BasicExample.dir/depend:
	cd /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo /home/rothnald/dungeon-defense/external/bullet/examples/BasicDemo/CMakeFiles/App_BasicExample.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/App_BasicExample.dir/depend

