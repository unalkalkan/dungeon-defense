
#ifndef DYNAMICOBJECT_H
#define DYNAMICOBJECT_H

#include <glm/gtx/norm.hpp>
#include <GameObject.h>
#include <Shader.h>
#include <Model.h>

class DynamicObject : public GameObject{
public:
    Shader *shader;
    Model *objectModel;
    float Speed = 10.0f;

    DynamicObject(string modelPath, btVector3 position, btVector3 colShape);
    DynamicObject(string modelPath, btVector3 position, btVector3 colShape, btScalar _mass);

    void Move(btVector3 newPosition);
    void ScaleModel(glm::vec3 scale);
    void Rotate(glm::quat rotation);
    void SmoothMove(btVector3 _end);
    void SmoothRotate(glm::quat rotation, float turningSpeed);

    void Update();
    void Update(Shader &outlineShader);
    char* GetType();
    void SetType(char* _type);
private:
    void ConvertPosition();
    btTransform Transform;

    // Variables for interpolated movement
    void Move(glm::vec3 newPosition);
    void Interpolate();
    bool moving = false;
    float distance;
    float elapsed = 0.01f;
    glm::vec3 direction;
    glm::vec3 start;
    glm::vec3 end;
};

DynamicObject::DynamicObject(string modelPath, btVector3 position, btVector3 colShape) {
    shader = new Shader((char*)"Shaders/ModelVertexShader.glsl", (char*)"Shaders/ModelFragmentShader.glsl");
    objectModel = new Model((char *)modelPath.c_str());
    Position = glm::vec3(position.getX(), position.getY(), position.getZ());
    type = (char*)"DynamicObject";

    // Initialization of Physics of the object
    collisionShape = new btBoxShape(colShape);
    motionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), position));
    btVector3 localInertia(0,0,0);
    collisionShape->calculateLocalInertia(mass, localInertia);
    btRigidBody::btRigidBodyConstructionInfo rigidBodyCI(mass, motionState, collisionShape, localInertia);
    rigidBody = new btRigidBody(rigidBodyCI);
    rigidBody->setUserPointer(this);
    rigidBody->setActivationState( DISABLE_DEACTIVATION );
    physicsEngine.dynamicsWorld->addRigidBody(rigidBody);
}
DynamicObject::DynamicObject(string modelPath, btVector3 position, btVector3 colShape, btScalar _mass) {
    shader = new Shader((char*)"Shaders/ModelVertexShader.glsl", (char*)"Shaders/ModelFragmentShader.glsl");
    objectModel = new Model((char *)modelPath.c_str());
    Position = glm::vec3(position.getX(), position.getY(), position.getZ());
    type = (char*)"DynamicObject";

    // Initialization of Physics of the object
    collisionShape = new btBoxShape(colShape);
    motionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), position));
    btVector3 localInertia(0,0,0);
    mass = _mass;
    collisionShape->calculateLocalInertia(mass, localInertia);
    btRigidBody::btRigidBodyConstructionInfo rigidBodyCI(mass, motionState, collisionShape, localInertia);
    rigidBody = new btRigidBody(rigidBodyCI);
    rigidBody->setUserPointer(this);
    rigidBody->setActivationState( DISABLE_DEACTIVATION );
    physicsEngine.dynamicsWorld->addRigidBody(rigidBody);
}

// For external moving
void DynamicObject::Move(btVector3 newPosition){
    btTransform initialTransform = rigidBody->getWorldTransform();
    initialTransform.setOrigin(newPosition);
    rigidBody->setWorldTransform(initialTransform);
}

// For internal moving
void DynamicObject::Move(glm::vec3 newPosition){
    btTransform initialTransform = rigidBody->getWorldTransform();
    initialTransform.getOrigin() += (btVector3(newPosition.x, newPosition.y, newPosition.z));
    rigidBody->setWorldTransform(initialTransform);
}
void DynamicObject::ScaleModel(glm::vec3 scale){
    Scale = scale;
//    collisionShape->setLocalScaling(btVector3(scale.x / 4, scale.y / 4, scale.z / 4));
}
void DynamicObject::Rotate(glm::quat rotation){
    btTransform initialTransform = rigidBody->getWorldTransform();
    initialTransform.setRotation(btQuaternion(rotation.x, rotation.y, rotation.z, rotation.w));
    rigidBody->setWorldTransform(initialTransform);
}

void DynamicObject::SmoothMove(btVector3 _end){
    start = Position;
    end = glm::vec3(_end.getX(), _end.getY(), _end.getZ());
    distance = glm::distance2(Position, end);
    direction = glm::normalize(end - Position);
    moving = true;
}
void DynamicObject::Interpolate(){
    if(moving){
        Move(direction * Speed * elapsed);
        if(glm::distance2(start, Position) >= distance){
            moving = false;
        }
    }
}
void DynamicObject::SmoothRotate(glm::quat rotation, float turningSpeed = 0.5f){
    btTransform initialTransform = rigidBody->getWorldTransform();
    glm::quat interpRotation = glm::slerp(Quaternion, rotation, turningSpeed);
    initialTransform.setRotation(btQuaternion(interpRotation.w, interpRotation.x, interpRotation.y, interpRotation.z));
    rigidBody->setWorldTransform(initialTransform);
}

void DynamicObject::Update(){
    Interpolate();
    shader->use();
    shader->setVec3("viewPos", camera.Position);
    shader->setMat4("projection", projection);
    shader->setMat4("view", view);

    // Directional Light settings
    shader->setVec3("directionalLight.direction", -0.2f, -1.0f, -0.3f);
    shader->setVec3("directionalLight.ambient", 0.05f, 0.05f, 0.05f);
    shader->setVec3("directionalLight.diffuse", 0.4f, 0.4f, 0.4f);
    shader->setVec3("directionalLight.specular", 0.5f, 0.5f, 0.5f);
    shader->setFloat("shininess", 64.0f);

    // Point Light settings
    for(unsigned int i = 0; i < 4; i++){
        string pre = "pointLights[";
        string number = to_string(i);

        shader->setVec3(pre + number + "].position", pointLightPositions[i]);
        shader->setVec3(pre + number + "].ambient", 0.05f, 0.05f, 0.05f);
        shader->setVec3(pre + number + "].diffuse", 0.8f, 0.8f, 0.8f);
        shader->setVec3(pre + number + "].specular", 1.0f, 1.0f, 1.0f);
        shader->setFloat(pre + number + "].constant", 1.0f);
        shader->setFloat(pre + number + "].linear", 0.09);
        shader->setFloat(pre + number + "].quadratic", 0.032);
    }
    ConvertPosition();
    model = glm::mat4();
    model = glm::translate(model, Position); // translate it down so it's at the center of the scene
    model = model * glm::toMat4(Quaternion);
    model = glm::scale(model, Scale);
    shader->setMat4("model", model);
    objectModel->Draw(*shader);
}
void DynamicObject::Update(Shader &outlineShader){
    outlineShader.use();
    outlineShader.setMat4("projection", projection);
    outlineShader.setMat4("view", view);

    ConvertPosition();
    model = glm::mat4();
    model = glm::translate(model, Position); // translate it down so it's at the center of the scene
    model = model * glm::toMat4(Quaternion);
    model = glm::scale(model, glm::vec3(Scale.x * 1.1f, Scale.y * 1.1f, Scale.z * 1.1f)); // Slightly bigger

    outlineShader.setMat4("model", model);
    objectModel->Draw(outlineShader);
}
void DynamicObject::ConvertPosition(){
    Transform = rigidBody->getWorldTransform();
    // Position
    Position.x = Transform.getOrigin().getX();
    Position.y = Transform.getOrigin().getY();
    Position.z = Transform.getOrigin().getZ();
    // Rotation
    btQuaternion pQuat = Transform.getRotation();
    Quaternion = glm::quat(pQuat.getW(), pQuat.getX(), pQuat.getY(), pQuat.getZ());
}

char* DynamicObject::GetType() {
    return type;
}
void DynamicObject::SetType(char* _type){
    type = _type;
}

#endif //DYNAMICOBJECT_H
