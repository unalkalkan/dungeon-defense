#ifndef PHYSICS_H
#define PHYSICS_H
#define BIT(x) (1<<(x))

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <btBulletDynamicsCommon.h>
#include <BulletCollision/Gimpact/btGImpactCollisionAlgorithm.h>

class Physics {
public:
    // Physics world variables
    btBroadphaseInterface* broadphase;
    btDefaultCollisionConfiguration* collisionConfiguration;
    btCollisionDispatcher* dispatcher;
    btSequentialImpulseConstraintSolver* solver;
    btDiscreteDynamicsWorld* dynamicsWorld;

    void InitPhysics();
    void SetGravity();
    void DeleteResources();
};

void Physics::InitPhysics() {
    broadphase = new btDbvtBroadphase();
    collisionConfiguration = new btDefaultCollisionConfiguration();
    dispatcher = new btCollisionDispatcher(collisionConfiguration);
    btGImpactCollisionAlgorithm::registerAlgorithm(dispatcher); // FIX
    solver = new btSequentialImpulseConstraintSolver;
    dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, broadphase, solver, collisionConfiguration);
//    dynamicsWorld->updateAabbs();
}

void Physics::SetGravity(){
    dynamicsWorld->setGravity(btVector3(0, -10, 0));
}

void Physics::DeleteResources(){
    delete dynamicsWorld;
    delete solver;
    delete dispatcher;
    delete collisionConfiguration;
    delete broadphase;
}

enum collisiontypes {
    COL_NOTHING = 0, //<Collide with nothing
    COL_TERRAIN = BIT(0),
    COL_RAY = BIT(1), //<Collide only with ray
    COL_TOWER = BIT(2),
    COL_ENEMY = BIT(3),
    COL_GRID = BIT(4),
};

#endif
