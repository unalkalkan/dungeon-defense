#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <global.h>

#include <btBulletDynamicsCommon.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <iostream>

using namespace std;

class GameObject {
public:
    glm::vec3 Position = glm::vec3(0.0f, 0.0f, 0.0f);
    glm::vec3 Scale = glm::vec3(1.0f, 1.0f, 1.0f);
    glm::quat Quaternion;

    btRigidBody* rigidBody;
    btScalar mass = 1;
    GameObject();

    virtual void SetType(char* _type);
    virtual char* GetType();
    GameObject *parent;
protected:
    char* type;
    btDefaultMotionState* motionState;
    btCollisionShape* collisionShape;
};
GameObject::GameObject(){}

void GameObject::SetType(char* _type) {}
char* GameObject::GetType(){}
#endif
