#ifndef STATICOBJECT_H
#define STATICOBJECT_H

#include <GameObject.h>
#include <Shader.h>
#include <Model.h>

class StaticObject : public GameObject{
public:
    Shader *shader;
    Model *objectModel;

    void Update();
    StaticObject(string modelPath);
    void ScaleModel(glm::vec3 scale);
    char* GetType();
};
StaticObject::StaticObject(string modelPath) {
    shader = new Shader((char*)"Shaders/ModelVertexShader.glsl", (char*)"Shaders/ModelFragmentShader.glsl");
    objectModel = new Model((char *)modelPath.c_str());

    // Initialization of Physics
    motionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, -1.0f, 0)));
    collisionShape = new btStaticPlaneShape(btVector3(0, 1.0f, 0), 1);
//    btCompoundShape * a = new btCompoundShape();
//    btTransform t;
//    t.setIdentity();
//    a->addChildShape(t, collisionShape);
//    t.setIdentity();
//    t.setOrigin(btVector3(-0.1f, 2.1f, 25.0118f));
//    btBoxShape *box1 = new btBoxShape(btVector3(8.0f, 1.0f, 8.0f));
//    a->addChildShape(t, box1);
//    t.setIdentity();
//    t.setOrigin(btVector3(-0.1f, 1.2f, 15.5f));
//    glm::quat rot(glm::vec3(75.0f, 0.0f, 0.0f));
//    t.setRotation(btQuaternion(rot.x, rot.y, rot.z, rot.w));
//    btBoxShape *box2 = new btBoxShape(btVector3(8.0f, 1.0f, 2.0f));
//    a->addChildShape(t, box2);
    btRigidBody::btRigidBodyConstructionInfo staticRigidBodyCI(0, motionState, collisionShape, btVector3(0, 0, 0));
    rigidBody = new btRigidBody(staticRigidBodyCI);
    physicsEngine.dynamicsWorld->addRigidBody(rigidBody);
    ScaleModel(glm::vec3(0.5f, 0.5f, 0.5f));
    Position = glm::vec3(0.0f, 0.0f, 0.0f);
    rigidBody->setUserPointer(this);
}

char* StaticObject::GetType() {
    return (char *)"StaticObject";
}
void StaticObject::ScaleModel(glm::vec3 scale){
    Scale = scale;
    //collisionShape->setLocalScaling(btVector3(scale.x / 4, scale.y / 4, scale.z / 4));
}
void StaticObject::Update(){
    shader->use();
    shader->setVec3("viewPos", camera.Position);
    shader->setMat4("projection", projection);
    shader->setMat4("view", view);

    // Directional Light settings
    shader->setVec3("directionalLight.direction", -0.2f, -1.0f, -0.3f);
    shader->setVec3("directionalLight.ambient", 0.05f, 0.05f, 0.05f);
    shader->setVec3("directionalLight.diffuse", 0.4f, 0.4f, 0.4f);
    shader->setVec3("directionalLight.specular", 0.5f, 0.5f, 0.5f);
    shader->setFloat("shininess", 64.0f);

    // Point Light settings
    for(unsigned int i = 0; i < 4; i++){
        string pre = "pointLights[";
        string number = to_string(i);

        shader->setVec3(pre + number + "].position", pointLightPositions[i]);
        shader->setVec3(pre + number + "].ambient", 0.05f, 0.05f, 0.05f);
        shader->setVec3(pre + number + "].diffuse", 0.8f, 0.8f, 0.8f);
        shader->setVec3(pre + number + "].specular", 1.0f, 1.0f, 1.0f);
        shader->setFloat(pre + number + "].constant", 1.0f);
        shader->setFloat(pre + number + "].linear", 0.09);
        shader->setFloat(pre + number + "].quadratic", 0.032);
    }
    model = glm::mat4();
    model = glm::translate(model, Position); // translate it down so it's at the center of the scene
    model = glm::scale(model, Scale);

    shader->setMat4("model", model);
    objectModel->Draw(*shader);
}
#endif //STATICOBJECT_H
