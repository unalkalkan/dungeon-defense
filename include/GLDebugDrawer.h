// Helper class; draws the world as seen by Bullet.
// This is very handy to see it Bullet's world matches yours
// How to use this class :
// Declare an instance of the class :
// 
// dynamicsWorld->setDebugDrawer(&mydebugdrawer);
// Each frame, call it :
// mydebugdrawer.SetMatrices(ViewMatrix, ProjectionMatrix);
// dynamicsWorld->debugDrawWorld();

#include <Shader.h>
#include <global.h>

GLuint VBO, VAO;
class GLDebugDrawer : public btIDebugDraw {
public:
    Shader* lineShader;
    glm::mat4 view;
    glm::mat4 projection;

    GLDebugDrawer(const char* vertexShaderPath, const char* fragmentShaderPath){
        lineShader = new Shader(vertexShaderPath, fragmentShaderPath);
        m_debugMode = 1;
    }

    void InitShader(glm::mat4 &viewParameter, glm::mat4 &projectionParameter){
        view = viewParameter;
        projection = projectionParameter;
        lineShader->use();
        lineShader->setMat4("view", view);
        lineShader->setMat4("projection", projection);
    }
    virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3& color)
    {
        // Vertex data
        GLfloat points[12];

        points[0] = from.x();
        points[1] = from.y();
        points[2] = from.z();
        points[3] = color.x();
        points[4] = color.y();
        points[5] = color.z();

        points[6] = to.x();
        points[7] = to.y();
        points[8] = to.z();
        points[9] = color.x();
        points[10] = color.y();
        points[11] = color.z();

        glDeleteBuffers(1, &VBO);
        glDeleteVertexArrays(1, &VAO);

        glGenBuffers(1, &VBO);
        glGenVertexArrays(1, &VAO);
        glBindVertexArray(VAO);

        glBindBuffer(GL_ARRAY_BUFFER, VBO);
        glBufferData(GL_ARRAY_BUFFER, sizeof(points), &points, GL_STATIC_DRAW);

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
        glEnableVertexAttribArray(0);

        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
        glEnableVertexAttribArray(1);

        glBindVertexArray(0);
        glBindVertexArray(VAO);
        glDrawArrays(GL_LINES, 0, 2);
        glBindVertexArray(0);
    }
    virtual void drawBox(const btVector3& bbMin, const btVector3& bbMax, const btVector3& color) {
        std::cout << "Draw Box" << std::endl;
        drawLine(btVector3(bbMin.getX(), bbMin.getY(), bbMin.getZ()), btVector3(bbMax.getX(), bbMin.getY(), bbMin.getZ()), color);
        drawLine(btVector3(bbMax.getX(), bbMin.getY(), bbMin.getZ()), btVector3(bbMax.getX(), bbMax.getY(), bbMin.getZ()), color);
        drawLine(btVector3(bbMax.getX(), bbMax.getY(), bbMin.getZ()), btVector3(bbMin.getX(), bbMax.getY(), bbMin.getZ()), color);
        drawLine(btVector3(bbMin.getX(), bbMax.getY(), bbMin.getZ()), btVector3(bbMin.getX(), bbMin.getY(), bbMin.getZ()), color);
        drawLine(btVector3(bbMin.getX(), bbMin.getY(), bbMin.getZ()), btVector3(bbMin.getX(), bbMin.getY(), bbMax.getZ()), color);
        drawLine(btVector3(bbMax.getX(), bbMin.getY(), bbMin.getZ()), btVector3(bbMax.getX(), bbMin.getY(), bbMax.getZ()), color);
        drawLine(btVector3(bbMax.getX(), bbMax.getY(), bbMin.getZ()), btVector3(bbMax.getX(), bbMax.getY(), bbMax.getZ()), color);
        drawLine(btVector3(bbMin.getX(), bbMax.getY(), bbMin.getZ()), btVector3(bbMin.getX(), bbMax.getY(), bbMax.getZ()), color);
        drawLine(btVector3(bbMin.getX(), bbMin.getY(), bbMax.getZ()), btVector3(bbMax.getX(), bbMin.getY(), bbMax.getZ()), color);
        drawLine(btVector3(bbMax.getX(), bbMin.getY(), bbMax.getZ()), btVector3(bbMax.getX(), bbMax.getY(), bbMax.getZ()), color);
        drawLine(btVector3(bbMax.getX(), bbMax.getY(), bbMax.getZ()), btVector3(bbMin.getX(), bbMax.getY(), bbMax.getZ()), color);
        drawLine(btVector3(bbMin.getX(), bbMax.getY(), bbMax.getZ()), btVector3(bbMin.getX(), bbMin.getY(), bbMax.getZ()), color);
    }
    virtual void drawContactPoint(const btVector3 &, const btVector3 &, btScalar, int, const btVector3 &) {}
    virtual void reportErrorWarning(const char *message) {
        std::cout << message << std::endl;
    }
    virtual void draw3dText(const btVector3 &, const char *) {}
    virtual void setDebugMode(int p) {
        m_debugMode = p;
    }
    int getDebugMode(void) const {
        return m_debugMode;
    }
private:
    int m_debugMode;
};