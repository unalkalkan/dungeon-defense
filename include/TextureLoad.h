#include <glad/glad.h>
#include <iostream>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

class TextureLoad{
public:
    bool success;

    TextureLoad(const char* texturePath, bool upsideDown){
        // Generate and Bind texture
        glGenTextures(1, &textureID);
        glBindTexture(GL_TEXTURE_2D, textureID);
        // texture wrapping
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        // texture filtering
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    
        stbi_set_flip_vertically_on_load(upsideDown);
        unsigned char *data = stbi_load(texturePath, &width, &height, &nrChannels, 0);
        if(data){
            if (nrChannels == 1)
                format = GL_RED;
            else if (nrChannels == 3)
                format = GL_RGB;
            else if (nrChannels == 4)
                format = GL_RGBA;

            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, format, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);
            success = true;
        }else{
            std::cout << "Failed to load texture at path : " << texturePath << std::endl;
            success = false;
        }   
        stbi_image_free(data);
    }
    void use(){
        glBindTexture(GL_TEXTURE_2D, textureID);
    }
    int getTextureID(){
        return textureID;
    }

private:
    unsigned int textureID;
    int width, height, nrChannels;
    GLenum format;
};