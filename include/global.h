#ifndef GLOBAL_H
#define GLOBAL_H

#include <Physics.h>
#include <Camera.h>

#include <string>
#include <map>
#include <iterator>
#include <vector>

using namespace std;

const unsigned int screenWidth = 1024;
const unsigned int screenHeight = 768;

// Lightning
const glm::vec3 LIGHT_POSITION(5.2f, 4.0f, 5.0f);

// Target position
const glm::vec3 START_POSITION = glm::vec3(0.0f, 0.881551, -43.9187);
const glm::vec3 TARGET_POSITION = glm::vec3(0.0f, 3.87927f, 22.2552f);

// Texture Database
map<string, unsigned int> TextureIDs;

//VAOs
unsigned int cubeVAO;
unsigned int lightVAO;

// Physics
Physics physicsEngine;

// Camera
Camera camera(glm::vec3(8.75786f, 18.3817f, 25.3235f), glm::vec3(0.0f, 1.0f, 0.0f), -104.084f, -27.3861f);
//Camera camera(glm::vec3(8.61286f, 8.92049f, 9.33709f), glm::vec3(0.0f, 1.0f, 0.0f), -134.075f, -34.298f);

// Ingame Variables
unsigned int Money = 40;
unsigned int BoughtTurret = 0;
bool waveStarted = false;
float BASE_HEALTH = 200.0f;
bool GAME_OVER = false;

// Turret Variables
float COOLDOWN = 3.0f;
float TOWER_RADIUS = 5.0f;
float DAMAGE = 80.0f;

// Current Enemy and Wave Variables
int QUANTITY = 5;         // Quantity of minions per wave
float MINION_HEALTH = 100.0f;
float MINION_SPEED = 10.0f; // Modify speed
float MINION_DAMAGE = 10.0f;
int GOLD_PER_MINION = 5;
// Next Enemy and Wave Variables
int NEXT_QUANTITY = 5;         // Quantity of minions per wave
float NEXT_MINION_HEALTH = 100.0f;
float NEXT_MINION_SPEED = 10.0f; // Modify speed
float NEXT_MINION_DAMAGE = 10.0f;
int NEXT_GOLD_PER_MINION = 5;

// Statistics
float waveStatusPercent = 1.0f;
int killedMinionCounter = 0;
int currentAliveMinion = 0;

// Mouse Position
float mouse_x = screenWidth / 2;
float mouse_y = screenHeight / 2;

// Deltatime and frame
float deltaTime = 0.0f;
float lastFrame = 0.0f;

// Coordinate System and Camera
glm::mat4 model;
glm::mat4 view;
glm::mat4 projection;

// Debug Line definitions
Shader *lineShader;
GLuint lineVBO, lineVAO;
void DebugLine(glm::vec3 lineFrom, glm::vec3 lineTo, glm::vec3 color){
    lineShader->use();
    lineShader->setMat4("view", view);
    lineShader->setMat4("projection", projection);

    // Vertex data
    GLfloat points[12];

    points[0] = lineFrom.x;
    points[1] = lineFrom.y;
    points[2] = lineFrom.z;
    points[3] = color.x;
    points[4] = color.y;
    points[5] = color.z;

    points[6] = lineTo.x;
    points[7] = lineTo.y;
    points[8] = lineTo.z;
    points[9] = color.x;
    points[10] = color.y;
    points[11] = color.z;

    glDeleteBuffers(1, &lineVBO);
    glDeleteVertexArrays(1, &lineVAO);

    glGenBuffers(1, &lineVBO);
    glGenVertexArrays(1, &lineVAO);
    glBindVertexArray(lineVAO);

    glBindBuffer(GL_ARRAY_BUFFER, lineVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(points), &points, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)0);
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(0);
    glBindVertexArray(lineVAO);
    glDrawArrays(GL_LINES, 0, 2);
    glBindVertexArray(0);
}

// positions of the point lights
glm::vec3 pointLightPositions[] = {
        glm::vec3( 5.5f,  8.2f,  5.7f),
        glm::vec3( 7.3f,  8.3f, -9.0f),
        glm::vec3(-9.0f,  8.0f, -20.0f),
        glm::vec3( 5.0f,  8.0f, -8.0f)
};

// Object Vertex Data for Cube
float cubeVertices[] = {
        // Position           // TextureLoad   // Normal vectors
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,  0.0f,  0.0f, -1.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,  0.0f,  0.0f, -1.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,  0.0f,  0.0f, -1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,  0.0f,  0.0f, -1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,  0.0f,  0.0f, -1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,  0.0f,  0.0f,  1.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,  0.0f,  0.0f,  1.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 1.0f,  0.0f,  0.0f,  1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,  0.0f,  0.0f,  1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,  0.0f,  0.0f,  1.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 1.0f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, -1.0f,  0.0f,  0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, -1.0f,  0.0f,  0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, -1.0f,  0.0f,  0.0f,

        0.5f,  0.5f,  0.5f,  1.0f, 0.0f,  1.0f,  0.0f,  0.0f,
        0.5f,  0.5f, -0.5f,  1.0f, 1.0f,  1.0f,  0.0f,  0.0f,
        0.5f, -0.5f, -0.5f,  0.0f, 1.0f,  1.0f,  0.0f,  0.0f,
        0.5f, -0.5f, -0.5f,  0.0f, 1.0f,  1.0f,  0.0f,  0.0f,
        0.5f, -0.5f,  0.5f,  0.0f, 0.0f,  1.0f,  0.0f,  0.0f,
        0.5f,  0.5f,  0.5f,  1.0f, 0.0f,  1.0f,  0.0f,  0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 1.0f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,  0.0f, -1.0f,  0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,  0.0f, -1.0f,  0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,  0.0f, -1.0f,  0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 1.0f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,  0.0f,  1.0f,  0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,  0.0f,  1.0f,  0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,  0.0f,  1.0f,  0.0f
};
#endif
