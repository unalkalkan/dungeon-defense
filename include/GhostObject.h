#ifndef GHOSTOBJECT_H
#define GHOSTOBJECT_H

#include "GameObject.h"
#include "Model.h"

class GhostObject: public GameObject{
public:
    Shader *shader;
    Model *objectModel;

    GhostObject(string modelPath, btVector3 position, btVector3 colShape);
    void ScaleModel(glm::vec3 scale);
    void Update();
    void Update(Shader &outlineShader);

    void SetType(char* _type);
    char* GetType();
private:
    void ConvertPosition();
    btTransform Transform;
};

GhostObject::GhostObject(string modelPath, btVector3 position, btVector3 colShape){
    shader = new Shader((char*)"Shaders/ModelVertexShader.glsl", (char*)"Shaders/ModelFragmentShader.glsl");
    objectModel = new Model((char *)modelPath.c_str());
    Position = glm::vec3(position.getX(), position.getY(), position.getZ());

    collisionShape = new btBoxShape(colShape);
    motionState = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), position));
    btVector3 gridLocalInertia(0,0,0);
    mass = 0;
    collisionShape->calculateLocalInertia(mass, gridLocalInertia);
    btRigidBody::btRigidBodyConstructionInfo rigidBodyCI(mass, motionState, collisionShape, gridLocalInertia);
    rigidBody = new btRigidBody(rigidBodyCI);
    rigidBody->setUserPointer(this);
    rigidBody->setCollisionFlags(rigidBody->getCollisionFlags() | btRigidBody::CF_NO_CONTACT_RESPONSE);
    physicsEngine.dynamicsWorld->addRigidBody(rigidBody);
};

void GhostObject::Update(){
    shader->use();
    shader->setVec3("viewPos", camera.Position);
    shader->setMat4("projection", projection);
    shader->setMat4("view", view);

    // Directional Light settings
    shader->setVec3("directionalLight.direction", -0.2f, -1.0f, -0.3f);
    shader->setVec3("directionalLight.ambient", 0.05f, 0.05f, 0.05f);
    shader->setVec3("directionalLight.diffuse", 0.4f, 0.4f, 0.4f);
    shader->setVec3("directionalLight.specular", 0.5f, 0.5f, 0.5f);
    shader->setFloat("shininess", 64.0f);

    // Point Light settings
    for(unsigned int i = 0; i < 4; i++){
        string pre = "pointLights[";
        string number = to_string(i);

        shader->setVec3(pre + number + "].position", pointLightPositions[i]);
        shader->setVec3(pre + number + "].ambient", 0.05f, 0.05f, 0.05f);
        shader->setVec3(pre + number + "].diffuse", 0.8f, 0.8f, 0.8f);
        shader->setVec3(pre + number + "].specular", 1.0f, 1.0f, 1.0f);
        shader->setFloat(pre + number + "].constant", 1.0f);
        shader->setFloat(pre + number + "].linear", 0.09);
        shader->setFloat(pre + number + "].quadratic", 0.032);
    }
    ConvertPosition();
    model = glm::mat4();
    model = glm::translate(model, Position); // translate it down so it's at the center of the scene
    model = model * glm::toMat4(Quaternion);
    model = glm::scale(model, Scale);
    shader->setMat4("model", model);
    objectModel->Draw(*shader);
}
void GhostObject::ScaleModel(glm::vec3 scale){
    Scale = scale;
}
void GhostObject::ConvertPosition(){
    Transform = rigidBody->getWorldTransform();
    // Position
    Position.x = Transform.getOrigin().getX();
    Position.y = Transform.getOrigin().getY();
    Position.z = Transform.getOrigin().getZ();
    // Rotation
    btQuaternion pQuat = Transform.getRotation();
    Quaternion = glm::quat(pQuat.getW(), pQuat.getX(), pQuat.getY(), pQuat.getZ());
}

void GhostObject::SetType(char* _type){
    type = _type;
}
char* GhostObject::GetType(){
    return type;
}
#endif //PROJECT_GHOSTOBJECT_H
