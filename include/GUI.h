#ifndef GUI_H
#define GUI_H

#include <global.h>
#include <nanogui/nanogui.h>
#include <GridObject.h>
using namespace nanogui;

// Game variables
vector<GridObject*> gridObjects;
DynamicObject *selected;
DynamicObject *hovered;
GridObject *hoveredGrid;
Wave *currentWave;

FormHelper *infoForm;
FormHelper *waveForm;
FormHelper *baseForm;
ProgressBar* healthProgressBar;
FormHelper *finishForm;
nanogui::ref<Window> finishWindow;
Popup* finishPopup;

int turretAmount = 0;
unsigned int currentMoney = 0;
float currentBaseHealth = 0.0f;
float maxHealth = 200.0f;
float healthPercent = 0.0f;
int turretPrice = 20;
bool updateNextWave = false;

void BuyTurret();
void DestroyTurret();
void UpgradeTurret();

void StartWave();
void InitializeParameters();
void UpdateCurrentParameters();
void UpdateNextParameters();
void FinishGame();
void RestartGame();
void ClearScene();

void InitGUI(Screen *screen) {
    infoForm = new FormHelper(screen);
    nanogui::ref<Window> turretsWindow = infoForm->addWindow(Eigen::Vector2i(screenWidth-190, 30), "Dungeon Info");
    infoForm->addGroup("Money");
    infoForm->addVariable("Money", currentMoney);
    infoForm->addGroup("Turrets");
    infoForm->addVariable("Amount of Turret", turretAmount);
    infoForm->addVariable("Turret Damages", DAMAGE);
    infoForm->addVariable("Turret Cooldown", COOLDOWN);
    infoForm->addVariable("Turret Range", TOWER_RADIUS);
    infoForm->addGroup("Market");
    infoForm->addButton("Buy Turret", GUI_H::BuyTurret);
//    infoForm->addButton("Upgrade Turrets", GUI_H::DestroyTurret);
//    infoForm->addButton("Destroy Turret", GUI_H::DestroyTurret);

    InitializeParameters();
    waveForm = new FormHelper(screen);
    nanogui::ref<Window> waveWindow = waveForm->addWindow(Eigen::Vector2i(18, 30), "Enemy Wave");
    waveForm->addGroup("Current Wave");
    waveForm->addVariable("Minion Health", MINION_HEALTH);
    waveForm->addVariable("Minion Speed", MINION_SPEED);
    waveForm->addVariable("Minion Damage", MINION_DAMAGE);
    waveForm->addVariable("Gold per Minion", GOLD_PER_MINION);
    waveForm->addVariable("Amount of Minion", QUANTITY);
    UpdateNextParameters();
    waveForm->addGroup("Next Wave");
    waveForm->addVariable("Minion Health", NEXT_MINION_HEALTH);
    waveForm->addVariable("Minion Speed", NEXT_MINION_SPEED);
    waveForm->addVariable("Minion Damage", NEXT_MINION_DAMAGE);
    waveForm->addVariable("Gold per Minion", NEXT_GOLD_PER_MINION);
    waveForm->addVariable("Amount of Minion", NEXT_QUANTITY);
    waveForm->addButton("Start Current Wave", GUI_H::StartWave);

    maxHealth = BASE_HEALTH;
    currentBaseHealth = BASE_HEALTH;
    healthPercent = currentBaseHealth / maxHealth;
    baseForm = new FormHelper(screen);
    nanogui::ref<Window> baseWindow = baseForm->addWindow(Eigen::Vector2i((screenWidth/2)-300, screenHeight-120), "Your Health");
    baseWindow->setLayout(new GroupLayout());
    baseWindow->setFixedSize(Eigen::Vector2i(600, 60));
    healthProgressBar = new ProgressBar(baseWindow);
    healthProgressBar->setValue(healthPercent);

    finishForm = new FormHelper(screen);
    int xsize = 400, ysize = 130;
    finishWindow = finishForm->addWindow(Eigen::Vector2i((screenWidth/2) - xsize, (screenHeight/2) - ysize), "Game Over");
    finishWindow->setLayout(new GroupLayout());
    finishWindow->setFixedSize(Eigen::Vector2i(xsize, ysize));
    Button *restartButton = new Button(finishWindow, "Restart");
    restartButton->setCallback(GUI_H::RestartGame);
    Button *finishButton = new Button(finishWindow, "Exit the Game");
    finishButton->setCallback(GUI_H::FinishGame);
    finishWindow->center();
    finishWindow->setVisible(false);

    screen->setVisible(true);
    screen->performLayout();
}

void UpdateGUI(){
    // Check Turrets
    int currentTurretAmount = 0;
    for(unsigned int i = 0; i < gridObjects.size(); i++){
        if(!gridObjects[i]->empty){
            currentTurretAmount++;
        }
    }
    if(currentTurretAmount != turretAmount){
        turretAmount = currentTurretAmount;
        infoForm->refresh();
    }

    // Check money
    if(Money != currentMoney){
        currentMoney = Money;
        infoForm->refresh();
    }

    // Check health
    if(BASE_HEALTH != currentBaseHealth){
        currentBaseHealth = BASE_HEALTH;
        healthPercent = currentBaseHealth / maxHealth;
        healthProgressBar->setValue(healthPercent);
        baseForm->refresh();
        if(healthPercent <= 0){
            GAME_OVER = true;
            finishWindow->setVisible(true);
        }
    }
    // Update next wave variables
    if(updateNextWave && !waveStarted){
        updateNextWave = false;
        UpdateCurrentParameters();
        UpdateNextParameters();
    }
}
void BuyTurret(){
    if(!waveStarted && !GAME_OVER){
        if(Money >= turretPrice){
            cout << "Just bought a turret." << endl;
            Money -= turretPrice;
            BoughtTurret++;
        }
    }
}
void UpgradeTurret(){
    if(turretAmount > 0){
        cout << "Turrets Upgraded." << endl;
    }
}
void DestroyTurret(){
    if(turretAmount > 0){
        cout << "Destroyed a turret." << endl;
        Money += 2 * (turretPrice/3);
    }
}

void StartWave(){
    if(!waveStarted && !GAME_OVER){
        currentWave = new Wave();
        for(int i = 0; i < gridObjects.size(); i++){
            gridObjects[i]->InitWave(currentWave);
        }
        waveStarted = true;
        updateNextWave = true;
        cout << "Wave Started." << endl;
    }
}
void InitializeParameters(){
    // Minion Variables
    QUANTITY = 5;
    MINION_HEALTH = 50.0f;
    MINION_SPEED = 20.0f;
    MINION_DAMAGE = 10.0f;
    GOLD_PER_MINION = 5;
    // InGame Variables
    BASE_HEALTH = 200.0f;
    Money = 40;
    BoughtTurret = 0;
    turretAmount = 0;
    currentMoney = 0;
    currentBaseHealth = 0.0f;
    maxHealth = 200.0f;
    healthPercent = 0.0f;
    turretPrice = 20;
    // Turret Variables
    COOLDOWN = 3.0f;
    TOWER_RADIUS = 5.0f;
    DAMAGE = 80.0f;
}
void UpdateNextParameters(){
    // Set parameters for the next run
    NEXT_QUANTITY += QUANTITY/3;
    NEXT_MINION_HEALTH += MINION_HEALTH/3;
    NEXT_MINION_SPEED += MINION_SPEED/5.0f;
    NEXT_MINION_DAMAGE += MINION_DAMAGE/3;
    NEXT_GOLD_PER_MINION += GOLD_PER_MINION/3;
    waveForm->refresh();
}
void UpdateCurrentParameters(){
    // Set Parameters for the current run
    QUANTITY = NEXT_QUANTITY;
    MINION_HEALTH = NEXT_MINION_HEALTH;
    MINION_SPEED = NEXT_MINION_SPEED;
    MINION_DAMAGE = NEXT_MINION_DAMAGE;
    GOLD_PER_MINION = NEXT_GOLD_PER_MINION;
    waveForm->refresh();
}
void FinishGame(){
    glfwTerminate();
}
void RestartGame(){
    GAME_OVER = false;
    waveStarted = false;
    ClearScene();
    finishWindow->setVisible(false);
    InitializeParameters();
    UpdateNextParameters();
    waveForm->refresh();
    baseForm->refresh();
    infoForm->refresh();
}
void ClearScene(){
//     Clear minions.
//    for(unsigned int i = 0; i < currentWave->minions.size(); i++){
//        currentWave->RemoveMinion(currentWave->minions[i]);
//    }
    // Clear turrets.
    /*for(unsigned int i = 0; i< gridObjects.size(); i++){
        if(!gridObjects[i]->empty)
            gridObjects[i]->RemoveTower();
    }*/
}
#endif //GUI_H
