#ifndef CAMERA_H
#define CAMERA_H

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

enum Camera_Movement {
    FORWARD, // Or UP
    BACKWARD, // Or DOWN
    LEFT,
    RIGHT,
};
enum Camera_Type {
    FIXED,
    FREE
};

const float YAW = -104.084f;
const float PITCH = -27.3861f;
const float SPEED = 5.0f;
const float SENSITIVITY = 0.1f;
const glm::vec3 FIXED_POSITION = glm::vec3(8.75786f, 18.3817f, 25.3235f);

class Camera{
public:
    // Camera Attributes
    glm::vec3 Position;
    glm::vec3 Front;
    glm::vec3 Right;
    glm::vec3 Up;
    glm::vec3 WorldUp;
    Camera_Type CameraType;

    // Euler Angles
    float Yaw;
    float Pitch;
    // Camera options
    float MovementSpeed;
    float MouseSensitivity;
    
    // Constructor with vectors
    Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH, Camera_Type cameratype = FIXED) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY){
        Position = position;
        WorldUp = up;
        Yaw = yaw;
        Pitch = pitch;
        CameraType = cameratype;
        updateCameraVectors();
    }
    // Constructor with scalar values
    Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch, Camera_Type cameratype) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY){    
        Position = glm::vec3(posX, posY, posZ);
        WorldUp = glm::vec3(upX, upY, upZ);
        Yaw = yaw;
        Pitch = pitch;
        CameraType = cameratype;
        updateCameraVectors();
    }
    glm::mat4 GetViewMatrix(){
        return glm::lookAt(Position, Position + Front, Up);
    }
    void ProcessKeyboard(Camera_Movement direction, float deltaTime){
        float velocity = MovementSpeed * deltaTime;
        if(CameraType == FIXED){
            if (direction == FORWARD)
                Position += Up * velocity;
            if (direction == BACKWARD)
                Position -= Up * velocity;
            if (direction == LEFT)
                Position -= Right * velocity;
            if (direction == RIGHT)
                Position += Right * velocity;
        }else{
            if (direction == FORWARD)
                Position += Front * velocity;
            if (direction == BACKWARD)
                Position -= Front * velocity;
            if (direction == LEFT)
                Position -= Right * velocity;
            if (direction == RIGHT)
                Position += Right * velocity;
        }
//        std::cout << "Position : " << Position.x << " " << Position.y << " " << Position.z << std::endl;
    }

    void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true){
        if(CameraType == FREE){
            xoffset *= MouseSensitivity;
            yoffset *= MouseSensitivity;
            Yaw += xoffset;
            Pitch += yoffset;

            if(constrainPitch){
                if(Pitch > 89.0f)
                    Pitch = 89.0f;
                if(Pitch < -89.0f)
                    Pitch = -89.0f;
            }
            updateCameraVectors();
        }
    }
    void ChangeCameraType(){
        CameraType = CameraType == FIXED ? FREE : FIXED;
        if(CameraType == FIXED){
            Position = FIXED_POSITION;
            Yaw = YAW;
            Pitch = PITCH;
            updateCameraVectors();
        }
    }
private:
    void updateCameraVectors(){
        glm::vec3 front;
        front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
        front.y = sin(glm::radians(Pitch));
        front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
        Front = glm::normalize(front);
        // Recalculate right and up vectors
        Right = glm::normalize(glm::cross(Front, WorldUp));
        Up = glm::normalize(glm::cross(Right, Front));

//        std::cout << "Yaw : " << Yaw << ", Pitch : " << Pitch << std::endl;
    }
};

#endif